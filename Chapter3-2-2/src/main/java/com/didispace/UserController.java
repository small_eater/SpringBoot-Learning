package com.didispace;

import com.didispace.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by dell on 2017/9/13.
 */
@RestController
public class UserController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/redis/string", method = RequestMethod.GET)
    public void insertString() {
        stringRedisTemplate.opsForValue().set("stringKey", "stringValue");
    }

    @RequestMapping(value = "/redis/string/object", method = RequestMethod.GET)
    public void insertStringObject() {
        User user = new User();
        user.setId(1L);
        user.setName("user1");
        user.setAge(22);
        redisTemplate.opsForValue().set("stringKeyObject", user);
    }

    @RequestMapping(value = "/redis/string/object/get", method = RequestMethod.GET)
    public User getStringObject() {
        User user = (User) redisTemplate.opsForValue().get("stringKeyObject");
        return user;
    }
    @RequestMapping(value = "/redis/string/object/delete", method = RequestMethod.GET)
    public void deleteStringObject() {
        redisTemplate.delete("stringKeyObject");
    }


}
