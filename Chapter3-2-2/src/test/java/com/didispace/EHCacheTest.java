package com.didispace;

import com.didispace.domain.User;
import com.didispace.domain.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

/**
 * Created by dell on 2017/9/13.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class EHCacheTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CacheManager cacheManager;

    @Before
    public void before() {
        userRepository.save(new User("AAA", 10));
        userRepository.save(new User("BBB", 11));
        userRepository.save(new User("CC", 12));
    }

    @Test
    public void test() throws Exception {
        User u1 = userRepository.findByName("AAA");
        System.out.println("第一次查询：" + u1.getAge());

        User u2 = userRepository.findByName("BBB");
        System.out.println("第二次查询：" + u2.getAge());

        User u3 = userRepository.findByName("CC");
        System.out.println("第3次查询：" + u3.getAge());

        userRepository.deleteByName("CC");

        User u4 = userRepository.findByName("CC");
        if(u4 != null) {
            System.out.println("第4次查询：" + u4.getAge());
        }else{
            System.out.println("第4次查询：");
        }


    }

}
